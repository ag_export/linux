#!/bin/bash

IDE=$1

if [ ! $IDE ]; then
	echo "usage: $0 <device-name>"
	exit 1
fi

SRC=$IDE.dts
TMP=$IDE.tmp.dts
DST=$IDE.dtb
DTC=../../../../scripts/dtc/dtc

echo "-- compiling $SRC"
cpp -nostdinc -I include -undef -x assembler-with-cpp $SRC > $TMP
$DTC -O dtb -b 0 -o $DST $TMP

echo "-- decompiling $DST to $IDE.decompiled.dts with removed phandles"
$DTC -O dts -s -o $IDE.decompiled.dts.tmp $DST
cat $IDE.decompiled.dts.tmp | grep -vE "\sphandle|\slinux,phandle" > $IDE.decompiled.dts
rm $IDE.decompiled.dts.tmp
rm $TMP

exit 0
